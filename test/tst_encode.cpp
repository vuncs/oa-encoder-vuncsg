#include <catch.hpp>

#include <QString>
#include <QTextStream>
#include <QIODevice>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include <encoder.h>

bool loadCodeTable(Encoder &encoder)
{
    bool result = false;
	QStringList fileContent;

    try
    {
        QFile file("../resources/codetable.json");        
        if(result = file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream fin(&file);
            while(!fin.atEnd())
            {
                fileContent.append(file.readLine());
            }
            file.close();
        }
    }
    catch (const std::exception& e) 
    {
        result = false;
    }

	if (result)
	{
		QJsonDocument jsonDoc = QJsonDocument::fromJson(fileContent.join("").toUtf8());
		if (!jsonDoc.isNull())
		{
			return (EncoderError::NoError == encoder.setCodeTable(jsonDoc.object()));
		}
	}


    return result;    
}

TEST_CASE( "Encode_ZeroLengthInput_NoErrorEmptyOutput" ) 
{
	Encoder encoder;

	REQUIRE( loadCodeTable(encoder) == true );

	QString resultString;
	QString inputString = "";
	EncoderError result = encoder.code(true, inputString, resultString);

	REQUIRE( result == EncoderError::NoError );
	REQUIRE( resultString == "" );
}

TEST_CASE( "Encode_ValidOneCharacterInput_NoErrorEncodedOutput_001" ) 
{
	Encoder encoder;

	REQUIRE( loadCodeTable(encoder) == true );

	QString resultString;
	QString inputString = "a";
	EncoderError result = encoder.code(true, inputString, resultString);

	REQUIRE( result == EncoderError::NoError );
	REQUIRE( resultString == "#asd" );
}

TEST_CASE( "Encode_InvalidOneCharacterInput_ErrorEmptyOutput_001" ) 
{
	Encoder encoder;

	REQUIRE( loadCodeTable(encoder) == true );

	QString resultString;
	QString inputString = "x";
	EncoderError result = encoder.code(true, inputString, resultString);

	REQUIRE( result == EncoderError::EncodeDecode );
	REQUIRE( resultString == "" );
}

TEST_CASE( "Encode_ValidTwoCharacterInput_NoErrorEncodedOutput_001" ) 
{
	Encoder encoder;

	REQUIRE( loadCodeTable(encoder) == true );

	QString resultString;
	QString inputString = "a ";
	EncoderError result = encoder.code(true, inputString, resultString);

	REQUIRE( result == EncoderError::NoError );
	REQUIRE( resultString == "#asd###" );
}

TEST_CASE( "Encode_ValidOneCharacterInput_NoErrorEncodedOutput_002" ) 
{
	Encoder encoder;

	REQUIRE( loadCodeTable(encoder) == true );

	QString resultString;
	QString inputString = "A";
	EncoderError result = encoder.code(true, inputString, resultString);

	REQUIRE( result == EncoderError::NoError );
	REQUIRE( resultString == "#asd" );
}

TEST_CASE( "Encode_OneValidOneInvalidCharacterInput_ErrorEmptyOutput__001" ) 
{
	Encoder encoder;

	REQUIRE( loadCodeTable(encoder) == true );

	QString resultString;
	QString inputString = "Am";
	EncoderError result = encoder.code(true, inputString, resultString);

	REQUIRE( result == EncoderError::EncodeDecode );
	REQUIRE( resultString == "" );
}

TEST_CASE( "Encode_ValidLongInput_NoErrorEncodedOutput_001" ) 
{
	Encoder encoder;

	REQUIRE( loadCodeTable(encoder) == true );

	QString resultString;
	QString inputString = "Abc $.df. aBDcF";
	EncoderError result = encoder.code(true, inputString, resultString);

	REQUIRE( result == EncoderError::NoError );
	REQUIRE( resultString == "#asd$something###@?darksidegummybear?####asd$darksidesomethinggummybear" );
}

TEST_CASE( "Encode_InvalidLongInput_ErrorEmptyOutput_001" ) 
{
	Encoder encoder;

	REQUIRE( loadCodeTable(encoder) == true );

	QString resultString;
	QString inputString = "Abc $.df. aBDcFjke";
	EncoderError result = encoder.code(true, inputString, resultString);

	REQUIRE( result == EncoderError::EncodeDecode );
	REQUIRE( resultString == "" );
}