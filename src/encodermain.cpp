#include <QString>
#include <QTextStream>
#include <QIODevice>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <encodermain.h>
#include <constants.h>

QTextStream cin(stdin);
QTextStream cout(stdout);
QTextStream cerr(stderr);


EncoderMain::EncoderMain(QObject *parent) :
    QObject(parent)
{
    app = QCoreApplication::instance();
	Encoder encoder;
}


void EncoderMain::coding(bool encode, const QString &input)
{
	QString output;
	
	if (encoder.code(encode, input, output) == EncoderError::NoError)
	{
		cout << output << endl;
	}
	else
	{
		cout << "INVALID INPUT" << endl;
	}	
}

void EncoderMain::codingFile(bool encode, const QString &inputFile, const QString &outputFile)
{
	switch(EncoderError result = codeFile(encode, inputFile, outputFile))
	{
		case EncoderError::NoError:
			cout << "done..." << endl;	
			return;
		case EncoderError::FileOpen:
			cout << "INVALID INPUT FILE" << endl;
			return;
		case EncoderError::FileSave:
			cout << "FILE SAVE ERROR" << endl;
			return;			
		case EncoderError::EncodeDecode:
			cout << "INVALID INPUT FILE CONTENT" << endl;
			return;
	}
}

bool EncoderMain::doCommand(const EncoderCommand &command, const QString &input)
{
	bool fileCommand = false;
	bool codingDirection = ENCODE;;

	switch(command)
	{
		case EncoderCommand::Exit:
			return false;
		case EncoderCommand::Encode:
			break;
		case EncoderCommand::Decode:
			codingDirection = DECODE;
			break;
		case EncoderCommand::EncodeFile:
			fileCommand = true;
			break;
		case EncoderCommand::DecodeFile:
			codingDirection = DECODE;
			fileCommand = true;
			break;
	}

	if (fileCommand)
	{
		QStringList inputs = input.split(' ', QString::SplitBehavior::SkipEmptyParts);

		if (inputs.count() == 2)
		{
			codingFile(codingDirection, inputs[0], inputs[1]);
		}
		else
		{
			cout << QString("Invalid command parameters! (%1)").arg(input) << endl;
		}
	}
	else
	{
		coding(codingDirection, input);		
	}
	return true;
}


QString EncoderMain::getUserInput()
{
	return cin.readLine().trimmed();
}

void EncoderMain::run()
{
	bool listen = true;
	QString input;
	EncoderError jsonError;

	do
	{
		cout << "Please, enter the path of codetable json file!" << endl;		
		input = getUserInput();
		jsonError = loadCodeTable(input);
		if (EncoderError::InvalidCodeTable == jsonError)
		{
			cout << "ERROR: Invalid coding table" << endl;		
		}
	} while (jsonError != EncoderError::NoError);

	cout << "CodeTable loaded." << endl;

	while (listen)
	{
		input = getUserInput();
		
		bool found = false;
		QMap<EncoderCommand,QString>::const_iterator i;
		for (i = COMMANDS.begin(); ((i != COMMANDS.end()) && !found); i++)
		{
			if (found = input.startsWith(i.value()))
			{
				input = input.right(input.length() - i.value().length()).trimmed();
				listen = doCommand(i.key(), input);
			}
		}
		if (!found)
		{
			cout << QString("Invalid command! (%1)").arg(input) << endl;
		}
	}

	cout << "KTHXBYE" << endl;

	emit finished();
}

bool EncoderMain::loadTextFile(const QString &filename, QStringList &fileContent)
{
    try
    {
        fileContent.clear();

        QFile file(filename);        
        if(file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream fin(&file);
            while(!fin.atEnd())
            {
                fileContent.append(file.readLine());
            }
            file.close();
			
			return true;
        }

		return false;
    }
    catch (const std::exception& e) 
    {
        cerr << e.what() << endl;        
        return false;
    }
}

bool EncoderMain::saveTextFile(const QString &filename, const QStringList &fileContent)
{
    try
    {
        QFile file(filename);  
        file.open(QIODevice::Truncate | QIODevice::Text | QIODevice::WriteOnly);
        QTextStream fout(&file);

        Q_FOREACH(auto line, fileContent)
        {
            fout << line << endl;
        }
        file.close();
		
		return true;
    }
    catch (const std::exception& e) 
    {
        cerr << e.what() << endl;        
        return false;
    }
}

EncoderError EncoderMain::loadCodeTable(const QString &filename)
{
	QStringList fileContent;
	if (loadTextFile(filename, fileContent))
	{
		QJsonDocument jsonDoc = QJsonDocument::fromJson(fileContent.join("").toUtf8());
		if (!jsonDoc.isNull())
		{
			return encoder.setCodeTable(jsonDoc.object());
		}
	}
	
    return EncoderError::InvalidCodeTable;
}

EncoderError EncoderMain::codeFile(const bool encode, const QString &source, const QString &dest)   
{
    QString output;
    bool result;

	QStringList sourceFileContent;

	if (loadTextFile(source, sourceFileContent))
    {
		QStringList destFileContent;

        Q_FOREACH(auto line, sourceFileContent)
        {
            line.remove(QRegExp("[\n\r]"));

            if(EncoderError::NoError == (encoder.code(encode, line, output)))
            {
                destFileContent.append(output);
            }
            else
            {
                destFileContent.clear();
                return EncoderError::EncodeDecode;    
            }
        }

        return (saveTextFile(dest, destFileContent) ? EncoderError::NoError : EncoderError::FileSave);
    }

    return EncoderError::FileOpen;
}
