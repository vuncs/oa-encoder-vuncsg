#pragma once

#include <QString>
#include <QMap>

const bool ENCODE = true;
const bool DECODE = false;

enum class EncoderError {NoError, InvalidCodeTable, EncodeDecode, FileOpen, FileSave};

enum class EncoderCommand {Exit, Encode, Decode, EncodeFile, DecodeFile};

static QMap<EncoderCommand, QString> commandsMap() {
    QMap<EncoderCommand, QString> map;
    map.insert(EncoderCommand::Exit, QString("EXIT"));
    map.insert(EncoderCommand::Encode, QString("ENCODE "));
    map.insert(EncoderCommand::Decode, QString("DECODE "));
    map.insert(EncoderCommand::EncodeFile, QString("ENCODE_FILE "));
    map.insert(EncoderCommand::DecodeFile, QString("DECODE_FILE "));
    return map;
} 

static const QMap<EncoderCommand, QString> COMMANDS = commandsMap();
