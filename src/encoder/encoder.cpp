#include <encoder.h>

Encoder::Encoder(QObject *parent) :
    QObject(parent)
{
    // nothing to do
}

EncoderError Encoder::setCodeTable(QJsonObject codeTableJsonObj)
{
    codetable.clear();

    foreach(auto x, codeTableJsonObj.keys())
    {
        if (codeTableJsonObj[x].isString())
        {
            codetable[x] = codeTableJsonObj[x].toString();
        }
        else
        {
            return EncoderError::InvalidCodeTable;
        }
    }    
    return EncoderError::NoError;
}

EncoderError Encoder::code(const bool &encode, const QString &source, QString &dest)
{
    dest = "";
    QString lSource = source;

    if (lSource.length() > 0)
    {
        while(lSource.length() > 0)
        {
            bool found = false;
            QMap<QString,QString>::iterator i;
            for (i = Encoder::codetable.begin(); ((i != Encoder::codetable.end()) && !found); i++)
            {
                QString tempkey = (encode ? i.key() : i.value());
                QString tempvalue = (encode ? i.value() : i.key());
                if (lSource.startsWith(tempkey, Qt::CaseInsensitive))
                {
                    dest += tempvalue;
                    found = true;
                    lSource = lSource.right(lSource.length() - tempkey.length());
                }
            }
            
            if (!found)
            {
                dest = "";
                return EncoderError::EncodeDecode;
            }
        }
    }

    return EncoderError::NoError;
}

