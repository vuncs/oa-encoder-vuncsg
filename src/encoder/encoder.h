#pragma once

#include <QObject>
#include <QString>
#include <QMap>
#include <QJsonObject>
#include <constants.h>

class Encoder : public QObject
{
    Q_OBJECT
private:
	QMap<QString,QString> codetable;

public:
    explicit Encoder(QObject *parent = 0);

    EncoderError setCodeTable(QJsonObject codeTable);

    EncoderError code(const bool &encode, const QString &source, QString &dest);
};
