#include <QCoreApplication>
#include <QTimer>

#include <encodermain.h>

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    EncoderMain encoderMain;

    QObject::connect(&encoderMain, SIGNAL(finished()), &app, SLOT(quit()));

    QTimer::singleShot(10, &encoderMain, SLOT(run()));
    return app.exec();
}