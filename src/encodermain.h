#pragma once

#include <QObject>
#include <QCoreApplication>
#include <QString>
#include <encoder.h>


class EncoderMain : public QObject
{
    Q_OBJECT
private:
    QCoreApplication *app;

	Encoder encoder;

    void coding(bool encode, const QString &input);
    void codingFile(bool encode, const QString &inputFile, const QString &outputFile);

	EncoderError codeFile(const bool encode, const QString &source, const QString &dest);


	bool doCommand(const EncoderCommand &command, const QString &input);

	bool loadTextFile(const QString &filename, QStringList &fileContent);
	bool saveTextFile(const QString &filename, const QStringList &fileContent);	
	EncoderError loadCodeTable(const QString &filename);

    QString getUserInput();

public:
    explicit EncoderMain(QObject *parent = 0);

    void endWork();

signals:
    void finished();

public slots:
    void run();
}; 


